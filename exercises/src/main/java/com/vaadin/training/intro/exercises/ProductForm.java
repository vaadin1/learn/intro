package com.vaadin.training.intro.exercises;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;

import java.util.List;

public class ProductForm extends VerticalLayout {
    public ProductForm() {
        NumberField nfPrice = new NumberField("Price");
        nfPrice.setStep(0.01);
        nfPrice.setSuffixComponent(new Span("€"));

        ComboBox<String> cmbCat = new ComboBox<>("Category", List.of("A", "B", "C"));

        HorizontalLayout hori1 = new HorizontalLayout();
        Button btnCancel = new Button("Cancel");
        Button btnSave = new Button("Save");
        btnSave.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        hori1.add(btnCancel);
        hori1.add(btnSave);

        add(new TextField("Name"));
        add(new TextArea("Description"));
        add(nfPrice);
        add(new DatePicker("Available"));
        add(cmbCat);
        add(hori1);
    }
}
